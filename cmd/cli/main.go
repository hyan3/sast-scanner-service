package main

import (
	"context"
	"fmt"
	"math"
	"os"
	"path/filepath"

	"github.com/rs/zerolog"
	"github.com/urfave/cli/v3"
	"gitlab.com/gitlab-org/secure/sast-scanner-service/logging"
	"gitlab.com/gitlab-org/secure/sast-scanner-service/scanner"
	"gitlab.com/gitlab-org/secure/sast-scanner-service/semgrep_lsp"
)

func main() {
	cmd := &cli.Command{
		Name:        "scanner-service",
		Description: "Host a SAST HTTP service",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "settings",
				Aliases:  []string{"s"},
				Usage:    "semgrep settings path",
				Required: false,
				Value:    "semgrep_settings.yml",
			},
			&cli.StringFlag{
				Name:     "config",
				Aliases:  []string{"c"},
				Usage:    "semgrep rules configuration file or directory",
				Required: true,
			},
			&cli.BoolFlag{
				Name:     "debug",
				Aliases:  []string{"d"},
				Usage:    "enable debug logging",
				Required: false,
				Value:    false,
			},
		},
		Arguments: []cli.Argument{&cli.StringArg{
			Name:      "input",
			UsageText: "input file(s)",
			Min:       1,
			Max:       math.MaxInt, // API is broken, should be able to use -1
		}},

		Action: func(ctx context.Context, cmd *cli.Command) error {
			var err error
			settings := cmd.String("settings")
			config := cmd.String("config")
			debug := cmd.Bool("debug")
			inputs := *cmd.Arguments[0].(*cli.StringArg).Values

			if debug {
				logging.Level(zerolog.DebugLevel)
			} else {
				logging.Level(zerolog.InfoLevel)
			}
			options := semgrep_lsp.SemgrepLSPOptions{Debug: debug}

			settings, err = filepath.Abs(settings)
			if err != nil {
				logging.Fatal().Err(err).Msg("settings file not found")
			}

			if _, err := os.Stat(settings); err == nil {
				options.Settings = settings
			}

			config, err = filepath.Abs(config)
			if err != nil {
				logging.Fatal().Err(err).Msg("config file not found")
			}

			sg, err := semgrep_lsp.NewSemgrepLSP(ctx, options)
			if err != nil {
				logging.Fatal().Err(err).Msg("failed to create semgrep LSP")
			}

			defer sg.Close()
			if err = sg.Start(); err != nil {
				logging.Fatal().Err(err).Msg("failed to start semgrep LSP")
			}

			err = sg.Initialize(
				[]string{config},
			)
			if err != nil {
				logging.Fatal().Err(err).Msg("failed to initialize semgrep LSP")
			}

			err = sg.RefreshRules()
			if err != nil {
				logging.Fatal().Err(err).Msg("failed to refresh semgrep LSP rules")
			}

			s, err := scanner.NewScanner(sg)
			if err != nil {
				logging.Fatal().Err(err).Msg("failed to start scanner")
			}
			defer s.Close()

			for _, inputfname := range inputs {
				inputContent, err := os.ReadFile(inputfname)
				if err != nil {
					logging.Fatal().Err(err).Msg("failed to read input file")
				}
				dlist, err := s.ScanFile(inputfname, inputContent)
				if err != nil {
					logging.Fatal().Err(err).Msg("failed to scan input file")
				}
				for _, d := range dlist {
					fmt.Printf("%s:%d:%d %s\n", inputfname, d.Range.Start.Line+1, d.Range.Start.Character+1, d.Code)
				}
			}
			logging.Debug().Msgf("done with all input files")

			sg.Stop()

			err = sg.Wait()
			if err != nil {
				logging.Fatal().Err(err).Msg("failed to wait on semgrep LSP")
			}

			return nil
		},
	}
	if err := cmd.Run(context.Background(), os.Args); err != nil {
		logging.Fatal().Err(err)
	}
}
