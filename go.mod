module gitlab.com/gitlab-org/secure/sast-scanner-service

go 1.22.3

require (
	github.com/gofrs/uuid v4.4.0+incompatible
	github.com/rs/zerolog v1.33.0
	github.com/urfave/cli/v3 v3.0.0-alpha9
	golang.org/x/exp/jsonrpc2 v0.0.0-20240529005216-23cca8864a10
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/exp/event v0.0.0-20240529005216-23cca8864a10 // indirect
	golang.org/x/sys v0.21.0 // indirect
	golang.org/x/xerrors v0.0.0-20231012003039-104605ab7028 // indirect
)
