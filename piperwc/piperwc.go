package piperwc

import (
	"errors"
	"io"
	"os/exec"
)

type pipeRWC struct {
	cmd        *exec.Cmd
	stdinPipe  io.WriteCloser
	stdoutPipe io.ReadCloser
	// stderrPipe io.ReadCloser
}

func (prwc *pipeRWC) Read(p []byte) (n int, err error) {
	return prwc.stdoutPipe.Read(p)
}

func (prwc *pipeRWC) Write(p []byte) (n int, err error) {
	return prwc.stdinPipe.Write(p)
}

func (prwc *pipeRWC) Close() error {
	err1 := prwc.stdinPipe.Close()
	err2 := prwc.stdoutPipe.Close()
	// err3 := prwc.stderrPipe.Close()

	// return errors.Join(err1, err2, err3)
	return errors.Join(err1, err2)
}

func NewPipeReadWriteCloser(cmd *exec.Cmd) (io.ReadWriteCloser, error) {
	p := &pipeRWC{}

	stdinPipe, err := cmd.StdinPipe()
	if err != nil {
		return nil, err
	}
	p.stdinPipe = stdinPipe

	stdoutPipe, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}
	p.stdoutPipe = stdoutPipe

	// stderrPipe, err := cmd.StderrPipe()
	// if err != nil {
	// 	return nil, err
	// }
	// p.stderrPipe = stderrPipe

	return p, nil
}
