package scanner

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/gofrs/uuid"
	"gitlab.com/gitlab-org/secure/sast-scanner-service/logging"
	"gitlab.com/gitlab-org/secure/sast-scanner-service/semgrep_lsp"
)

type Scanner struct {
	sg             *semgrep_lsp.SemgrepLSP
	tmpdir         string
	scanCond       *sync.Cond
	progressTokens map[string]bool
	tmp2inputfname map[string]string
	diagnostics    map[string][]semgrep_lsp.Diagnostic
	dlock          *sync.Mutex
}

func NewScanner(sg *semgrep_lsp.SemgrepLSP) (*Scanner, error) {
	s := &Scanner{
		sg:             sg,
		scanCond:       sync.NewCond(&sync.Mutex{}),
		progressTokens: make(map[string]bool),
		tmp2inputfname: make(map[string]string),
		diagnostics:    make(map[string][]semgrep_lsp.Diagnostic),
		dlock:          &sync.Mutex{},
	}

	tmpdir, err := os.MkdirTemp("/tmp", "sast-scanner-service-")
	if err != nil {
		return nil, err
	}
	s.tmpdir = tmpdir
	// errors past this point in the constructor must Close to clean up tmpdir

	sg.RegisterProgressHandler(s.progressHandler)
	sg.RegisterDiagnosticsHandler(s.diagnosticsHandler)

	err = s.sync()
	if err != nil {
		s.Close()
		return nil, err
	}
	return s, nil
}
func (s *Scanner) scanNotify() {
	s.scanCond.L.Lock()
	s.scanCond.Broadcast()
	s.scanCond.L.Unlock()
}

func (s *Scanner) scanWait(i int) {
	s.scanCond.L.Lock()
	for j := 0; j < i; j++ {
		s.scanCond.Wait()
	}
	s.scanCond.L.Unlock()
}

func (s *Scanner) progressHandler(ctx context.Context, progress *semgrep_lsp.ProgressParams) error {
	if progress.Value.Kind == "begin" {
		if progress.Value.Message == "Scanning Workspace" {
			s.progressTokens[progress.Token] = true
			logging.Debug().Msgf("progress begin %s begin %s", progress.Value.Message, progress.Token)
		}
	} else if progress.Value.Kind == "end" {
		if s.progressTokens[progress.Token] {
			delete(s.progressTokens, progress.Token)
			logging.Debug().Msgf("progress end %s", progress.Token)
			s.scanNotify()
		}
	}
	return nil
}

func (s *Scanner) diagnosticsHandler(ctx context.Context, diags *semgrep_lsp.DiagnosticsParams) error {
	tmpfname := diags.Uri
	if strings.HasPrefix(tmpfname, "file://") {
		tmpfname = tmpfname[7:]
	}
	inputfname, ok := s.tmp2inputfname[tmpfname]
	if !ok {
		return nil
	}

	dlist := diags.Diagnostics

	s.dlock.Lock()
	defer s.dlock.Unlock()

	odlist, ok := s.diagnostics[inputfname]
	if ok && len(odlist) > 0 {
		logging.Debug().Msgf("already have %d diags for %s", len(odlist), inputfname)
		return nil
	}
	s.diagnostics[inputfname] = dlist

	return nil
}

func (s *Scanner) Close() error {
	if s.tmpdir != "" {
		return os.RemoveAll(s.tmpdir)
	}
	return nil
}

func (s *Scanner) sync() error {
	var err error
	// synchronize with the language server by initializing a couple of scans by adding and remove an empty workspace
	err = s.sg.AddWorkspaceFolder(s.tmpdir)
	if err != nil {
		return err
	}
	err = s.sg.RemoveWorkspaceFolder(s.tmpdir)
	if err != nil {
		return err
	}
	s.scanWait(2)
	logging.Debug().Msgf("finished with startup")
	return nil
}
func (s *Scanner) ScanFile(inputfname string, inputContent []byte) ([]semgrep_lsp.Diagnostic, error) {
	var err error
	u := uuid.Must(uuid.NewV4())
	tmpfname := filepath.Join(s.tmpdir, u.String()+filepath.Ext(inputfname))
	s.tmp2inputfname[tmpfname] = inputfname
	if err = os.WriteFile(tmpfname, inputContent, 0644); err != nil {
		return nil, err
	}
	logging.Debug().Msgf("file copied %s", inputfname)
	err = s.sg.AddWorkspaceFolder(s.tmpdir)
	if err != nil {
		return nil, err
	}

	if err = s.sg.ScanWorkspace(); err != nil {
		return nil, err
	}

	err = s.sg.RemoveWorkspaceFolder(s.tmpdir)
	if err != nil {
		return nil, err
	}

	logging.Debug().Msgf("waiting for scan...")
	s.scanWait(3)

	// diagnostics, if any, will have been published by now so it's safe to remove the file
	delete(s.tmp2inputfname, tmpfname)
	logging.Debug().Msgf("deleting %s", inputfname)
	os.Remove(tmpfname)
	logging.Debug().Msgf("file deleted %s", inputfname)

	s.dlock.Lock()
	dlist, ok := s.diagnostics[inputfname]
	if ok {
		delete(s.diagnostics, inputfname)
	} else {
		fmt.Printf("%s was not scanned\n", inputfname)
	}
	s.dlock.Unlock()

	if err = s.sg.ScanWorkspace(); err != nil {
		return nil, err
	}

	return dlist, nil
}
