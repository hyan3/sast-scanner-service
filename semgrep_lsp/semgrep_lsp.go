package semgrep_lsp

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	path_ "path"
	"strconv"
	"sync/atomic"
	"time"

	"gitlab.com/gitlab-org/secure/sast-scanner-service/logging"
	"gitlab.com/gitlab-org/secure/sast-scanner-service/piperwc"
	"golang.org/x/exp/jsonrpc2"
)

type ResponseHandler func(context.Context, *jsonrpc2.Response) error
type RequestHandler func(context.Context, *jsonrpc2.Request) error
type NotificationHandler func(context.Context, *jsonrpc2.Request) error
type DiagnosticsHandler func(context.Context, *DiagnosticsParams) error
type ProgressHandler func(context.Context, *ProgressParams) error

var MethodNotFoundError = jsonrpc2.NewError(-32601, "method not found")

type SemgrepLSP struct {
	cmd  *exec.Cmd
	pipe io.ReadWriteCloser

	framer jsonrpc2.Framer

	// meassage reader and writer from framer
	reader jsonrpc2.Reader
	writer jsonrpc2.Writer

	ctx context.Context

	seq int64

	// when a request is made, a response handler is associated in order to asyncronously handle the server's response
	responseHandlers map[int64]ResponseHandler

	// incoming requests are handled according by handlers registered by method name
	requestHandlers       map[string]RequestHandler
	DefaultRequestHandler RequestHandler

	// same deal for notificaitons
	notificationHandlers       map[string]NotificationHandler
	DefaultNotificationHandler NotificationHandler

	initialized chan int

	running    bool
	doneSignal chan int

	cmdError      error
	readLoopError error
}

type SemgrepLSPOptions struct {
	Settings  string
	Debug     bool
	ExtraOpts []string
}

func NewSemgrepLSP(ctx context.Context, config SemgrepLSPOptions) (*SemgrepLSP, error) {
	var err error
	sg := &SemgrepLSP{}

	sg.ctx = ctx

	// Note: use --experimental to minimize python dependencies
	opts := append([]string{"lsp", "--experimental"}, config.ExtraOpts...)
	if config.Debug {
		opts = append(opts, "--debug")
	} else {
		opts = append(opts, "-q")
	}

	// Note: this calls the semgrep python wrapper.. we can call the semgrep-core binary directly, but it's argv[0] must be set to "osemgrep"
	sg.cmd = exec.Command("semgrep", opts...)
	if config.Debug {
		// TODO: can we wrap the semgrep output in zerolog Debug events?
		sg.cmd.Stderr = os.Stderr
	} else {
		sg.cmd.Stderr = nil
	}

	// Note: SEMGREP_SEND_METRICS doesn't work with the lsp subcommand of semgrep.. see the initialize notification.

	// Note: do not replace cmd.Env - ocaml uses path, e.g. for "uname"
	if config.Settings != "" {
		os.Setenv(
			"SEMGREP_SETTINGS_FILE",
			config.Settings)
	}

	sg.pipe, err = piperwc.NewPipeReadWriteCloser(sg.cmd)
	if err != nil {
		return nil, err
	}

	sg.framer = jsonrpc2.HeaderFramer()
	sg.reader = sg.framer.Reader(sg.pipe)
	sg.writer = sg.framer.Writer(sg.pipe)

	sg.seq = -1
	sg.responseHandlers = make(map[int64]ResponseHandler)

	sg.requestHandlers = make(map[string]RequestHandler)
	sg.DefaultRequestHandler = func(ctx context.Context, req *jsonrpc2.Request) error {
		logging.Debug().Msgf("unhandled request method: %s", req.Method)
		sg.WriteResponse([]byte(MethodNotFoundError.Error()), MethodNotFoundError, req.ID)
		return nil
	}

	sg.notificationHandlers = make(map[string]NotificationHandler)
	sg.DefaultNotificationHandler = func(ctx context.Context, req *jsonrpc2.Request) error {
		logging.Debug().Msgf("unhandled notification method: %s", req.Method)
		return nil
	}

	sg.initialized = make(chan int)

	return sg, err
}

func (sg *SemgrepLSP) WriteRequest(method string, params any, handler ResponseHandler) (int64, error) {
	id := atomic.AddInt64(&sg.seq, 1)
	sg.responseHandlers[id] = handler
	req, err := jsonrpc2.NewCall(jsonrpc2.Int64ID(id), method, params)
	if err != nil {
		return 0, err
	}
	return sg.writer.Write(sg.ctx, req)
}

func (sg *SemgrepLSP) Notify(method string, params any) (int64, error) {
	req, err := jsonrpc2.NewNotification(method, params)
	if err != nil {
		return 0, err
	}
	return sg.writer.Write(sg.ctx, req)
}

// see https://www.jsonrpc.org/specification for error codes
func (sg *SemgrepLSP) WriteResponse(result json.RawMessage, err error, id jsonrpc2.ID) (int64, error) {
	return sg.writer.Write(sg.ctx, &jsonrpc2.Response{
		Result: result,
		Error:  err,
		ID:     id,
	})
}

func (sg *SemgrepLSP) Read() (*jsonrpc2.Request, *jsonrpc2.Response, int64, error) {
	m, sz, err := sg.reader.Read(sg.ctx)
	if err != nil {
		return nil, nil, 0, err
	}
	switch m := m.(type) {
	case *jsonrpc2.Request:
		return m, nil, sz, nil
	case *jsonrpc2.Response:
		return nil, m, sz, nil
	}
	return nil, nil, 0, fmt.Errorf("unknown message type %T: %+v", m, m)
}

func (sg *SemgrepLSP) Start() error {
	sg.running = true
	sg.doneSignal = make(chan int)
	go func() {
		sg.readLoopError = sg.ReadLoop()
	}()
	return sg.cmd.Start()
}

func (sg *SemgrepLSP) Interrupt() error {
	return sg.cmd.Process.Signal(os.Interrupt)
}

func (sg *SemgrepLSP) Stop() error {
	sg.running = false

	// non blocking send to done channel
	select {
	case sg.doneSignal <- 1:
	default:
	}
	return nil
}

func (sg *SemgrepLSP) Wait() error {
	go func() {
		sg.cmdError = sg.cmd.Wait()
		sg.Stop()
	}()

	var doneSignaled bool
	for {
		select {
		case <-time.After(1 * time.Second):
		case <-sg.doneSignal:
			doneSignaled = true
		}
		if !sg.running || doneSignaled || sg.readLoopError != nil {
			sg.running = false
			return errors.Join(sg.cmdError, sg.readLoopError)
		}
	}
}

func (sg *SemgrepLSP) Close() error {
	return sg.pipe.Close()
}

func (sg *SemgrepLSP) ReadLoop() error {
	for {
		req, resp, _, err := sg.Read()
		if err != nil {
			return err
		} else {
			if req != nil {
				if req.ID.Raw() == nil {
					h, ok := sg.notificationHandlers[req.Method]
					if !ok {
						h = sg.DefaultNotificationHandler
					}
					if err = h(sg.ctx, req); err != nil {
						return err
					}
				} else {
					h, ok := sg.requestHandlers[req.Method]
					if !ok {
						h = sg.DefaultRequestHandler
					}
					if err = h(sg.ctx, req); err != nil {
						return err
					}
				}
			} else {
				if !resp.ID.IsValid() {
					return fmt.Errorf("response with invalid ID = nil!")
				} else {
					rid := resp.ID.Raw()
					var id int64
					// Note: our client uses integer ids
					switch rid := rid.(type) {
					case int64:
						id = rid
					case string:
						id, err = strconv.ParseInt(rid, 10, 64)
						if err != nil {
							return fmt.Errorf("response with unexpected ID = %s!", rid)
						}
					}
					handler, ok := sg.responseHandlers[id]
					if !ok {
						return fmt.Errorf("response to unkonwn request ID = %d!", id)
					} else {
						err = handler(sg.ctx, resp)
						if err != nil {
							return err
						}
					}
				}
			}
		}
	}
	// unreachable
}

// reset ruleset and workspace by issuing a new initialize method
func (sg *SemgrepLSP) Initialize(configuration []string) error {
	params := map[string]any{
		"clientInfo": map[string]any{
			"name":    "test",
			"version": "0.0.1",
		},
		"capabilities": map[string]any{
			"workspace": map[string]any{
				"didChangeWatchedFiles ": map[string]any{
					"dynamicRegistration":    true,
					"relativePatternSupport": false,
				},
			},
		},
		"initializationOptions": map[string]any{
			"scan": map[string]any{
				"configuration": configuration,
				"onlyGitDirty":  false,
				"ci":            false,

				"exclude": []string{},
				"include": []string{},

				"jobs":           1,
				"maxMemory":      0,
				"maxTargetBytes": 0,
			},
			"metrics": map[string]any{
				"enabled": false,
			},
		},
		"workspaceFolders": []map[string]any{},
	}

	_, err := sg.WriteRequest("initialize", &params, func(ctx context.Context, resp *jsonrpc2.Response) error {
		sg.initialized <- 1
		return nil
	})
	if err == nil {
		<-sg.initialized
	}
	return err
}

func (sg *SemgrepLSP) FileOpened(path, text string) error {
	langId := path_.Ext(path)[1:]
	params := map[string]any{
		"textDocument": map[string]any{
			"uri":        path,
			"languageId": langId,
			"version":    1,
			"text":       text,
		},
	}
	_, err := sg.Notify("textDocument/didOpen", params)
	return err
}

func (sg *SemgrepLSP) FileClosed(path string) error {
	params := map[string]any{
		"textDocument": map[string]any{
			"uri": path,
		},
	}
	_, err := sg.Notify("textDocument/didClose", params)
	return err
}

func (sg *SemgrepLSP) FileSaved(path, text string) error {
	params := map[string]any{
		"textDocument": map[string]any{
			"uri": path,
		},
		text: text,
	}
	_, err := sg.Notify("textDocument/didSave", params)
	return err
}

func (sg *SemgrepLSP) FileDeleted(path string) error {
	params := map[string]any{
		"files": []map[string]any{
			{
				"uri": path,
			},
		},
	}
	_, err := sg.Notify("workspace/didDeleteFiles", params)
	return err
}

func (sg *SemgrepLSP) AddWorkspaceFolder(path string) error {
	params := map[string]any{
		"event": map[string]any{
			"added": []map[string]any{
				{
					"name": path,
					"uri":  path,
				},
			},
			"removed": []map[string]any{},
		},
	}
	_, err := sg.Notify("workspace/didChangeWorkspaceFolders", params)
	return err
}

func (sg *SemgrepLSP) RemoveWorkspaceFolder(path string) error {
	params := map[string]any{
		"event": map[string]any{
			"added": []map[string]any{},
			"removed": []map[string]any{
				{
					"name": path,
					"uri":  path,
				},
			},
		},
	}
	_, err := sg.Notify("workspace/didChangeWorkspaceFolders", params)
	return err
}

func (sg *SemgrepLSP) RefreshRules() error {
	_, err := sg.Notify("semgrep/refreshRules", map[string]any{})
	return err
}

func (sg *SemgrepLSP) ScanWorkspace() error {
	_, err := sg.Notify("semgrep/scanWorkspace", map[string]any{
		"full": true,
	})
	return err
}

func (sg *SemgrepLSP) RegisterRequestHandler(method string, h RequestHandler) {
	sg.requestHandlers[method] = h
}

func (sg *SemgrepLSP) RegisterNotificationHandler(method string, h NotificationHandler) {
	sg.notificationHandlers[method] = h
}

type DiagnosticsParams struct {
	Diagnostics []Diagnostic `json:"diagnostics"`
	Uri         string       `json:"uri"`
}

type Position struct {
	Character int `json:"character"`
	Line      int `json:"line"`
}
type Range struct {
	Start Position `json:"start"`
	End   Position `json:"end"`
}

type Diagnostic struct {
	Code     string `json:"code"`
	Message  string `json:"message"`
	Range    Range  `json:"range"`
	Severity int    `json:"severity"`
	Source   string `json:"source"`
}

func (sg *SemgrepLSP) RegisterDiagnosticsHandler(h DiagnosticsHandler) {
	sg.RegisterNotificationHandler("textDocument/publishDiagnostics", func(ctx context.Context, req *jsonrpc2.Request) error {
		var d DiagnosticsParams
		if err := json.Unmarshal(req.Params, &d); err != nil {
			return err
		}
		return h(ctx, &d)
	})
}

type ProgressParams struct {
	Token string        `json:"token"`
	Value ProgressValue `json:"value"`
}
type ProgressValue struct {
	Kind    string `json:"kind"`
	Message string `json:"message,omitempty"`
	Title   string `json:"title,omitempty"`
}

func (sg *SemgrepLSP) RegisterProgressHandler(h ProgressHandler) {
	sg.RegisterNotificationHandler("$/progress", func(ctx context.Context, req *jsonrpc2.Request) error {
		var p ProgressParams
		if err := json.Unmarshal(req.Params, &p); err != nil {
			return err
		}
		return h(ctx, &p)
	})
}

func (sg *SemgrepLSP) Echo(message string, handler ResponseHandler) error {
	_, err := sg.WriteRequest("debug/echo", map[string]any{"message": message}, handler)
	return err
}
